Role Name
=========
ansible_borg_client

This role will setup a borg and a wrapper script borgbackup_runner on a client. 

What is Borg?
BorgBackup is a deduplicating backup program. Optionally, it supports compression and authenticated encryption. See https://borgbackup.readthedocs.io/en/stable/

What is borgbackup_runner?
In short, the borgbackup_runner initiates a backup of user defined local directories. After a successful backup, the borgbackup_runner will submit a user defined post job to the backup repo. Every minute the postjob_runner will check the queue for post job request and execute it. This is useful for testing backups. 

Additionally, borgbackup_runner has the ability to run a custom command prior to running the backup job. This is useful when backing up databases using a 3rd party utility like Holland. See Example playbook below on how to use this.

Requirements
------------
git, awk, and sed must be installed

Role Variables
--------------

```
borg_required_packages:
  yum:
    - borgbackup
borgbackup_runner_log_directory: /var/log/borgbackup
backup_user: borg
borg_repo_path: /backups
borgrunner_cron_daily: 23
borgrunner_pre_run_command: sudo holland bk
borgbackup_runner_install_path: /usr/local/borgbackup
borgbackup_runner_config_name: borgbackup_runner_config.yml
borg_home_directory: /home/borg
borg_repo_hostname: test_vm1 
postjob_runner_queue_path: /backups/postjobs
borg_repo_name: backups
borg_retention_policy:
  keep_hourly: 3
  keep_daily: 7
  keep_weekly: 4
  keep_monthly: 6
ansible_executor:
borg_pass:
```

Dependencies
------------

ansible_role_packages
ansible_role_repository

Example Playbook
----------------

```
- hosts: borgclients
  vars:
    borgrunner_pre_run_command: sudo holland bk
  roles:
     -  role: ansible_role_borg_client
        configure_borgbackup_client: true
```

License
-------

BSD

Author Information
------------------

The Development Range Engineering, Architecture, and Modernization (DREAM) Team
