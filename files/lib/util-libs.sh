#!/bin/bash

SCRIPT_LOG_DIR=/var/log/borgbackup
echo $borg_log_path


LogPost(){
 local function_name="${FUNCNAME[1]}"
    timeAndDate=`date`
    touch $SCRIPT_LOG_DIR/borgbackup_postrun_event.log
    if [ -n "${1}" ]; then
      IN="${1}"
      echo "[$timeAndDate] [Event]  $IN" >> $SCRIPT_LOG_DIR/borgbackup_postrun_event.log
    else
      while read IN
      do 
        echo "[$timeAndDate] [Event]  $IN" >> $SCRIPT_LOG_DIR/borgbackup_postrun_event.log
      done
    fi
}

LogInfo(){
 local function_name="${FUNCNAME[1]}"
    timeAndDate=`date`
    touch $SCRIPT_LOG_DIR/borgbackup_server.log
    if [ -n "${1}" ]; then
      IN="${1}"
      echo "[$timeAndDate] [Info]  $IN" >> $SCRIPT_LOG_DIR/borgbackup_server.log
    else
      while read IN
      do 
        echo "[$timeAndDate] [Info]  $IN" >> $SCRIPT_LOG_DIR/borgbackup_server.log
      done
    fi
}

LogError(){
 local function_name="${FUNCNAME[1]}"
    timeAndDate=`date`
    if [ -n "${1}" ]; then
      IN="${1}"
      echo "[$timeAndDate] [Error]  $IN" >> $SCRIPT_LOG_DIR/borgbackup_server.log
    else
      while read IN
      do 
        echo "[$timeAndDate] [Error]  $IN" >> $SCRIPT_LOG_DIR/borgbackup_server.log
      done
    fi
}

InputSanitize() {

  # Sanitizes input to a set of reasonable filename characters
  local requested_function_input="${1}";
  local function_input="${requested_function_input//[^A-Za-z0-9_.-]}";
  if [[ "${requested_function_input}" != "${function_input}" ]]; then
   echo "invalid value in configuration. exiting"
   exit
  fi
  if [[ -z "${requested_function_input}" ]]; then 
    echo "required value is empty. exiting"
    exit
  fi
}

CheckIfEmpty() {
  if [[ -z "${requested_function_input}" ]]; then 
    echo "required value is empty. exiting"
    exit
  fi
}

utilDeps() {

  # Ensures the dependency checker is actually installed before proceeding
  which which &>/dev/null \
    || { echo "[FAIL] Dependency (which) missing!" >&2; return 1; };

  # Sanitizes input to a set of reasonable filename characters
  local requested_function_input="${1}";
  local function_input="${requested_function_input//[^A-Za-z0-9_.-]}";
  if [[ "${requested_function_input}" != "${function_input}" ]]; then
    echo "[FAIL] Requested dependency or array name contains invalid characters; exiting." >&2;
    return 1;
  fi

  # Determines if $1 is a single dep or an array name by parsing the output of "declare -p"
  local input_type="$(declare -p "${function_input}" 2>/dev/null || echo "non_array" )";
  local input_type="${input_type%%=*}";
  local input_type="${input_type##declare -}";
  case "${input_type:0:1}" in
    a)
      local input_type="array";
      ;;
    *)
      local input_type="dep_name";
      ;;
  esac

  # Populates dep_array with either single dep name or contents of the provided array
  if [[ "${input_type}" == "array" ]]; then
    local dep_array=( $(eval echo \$\{${function_input}\[\@\]\}) );
  else
    local dep_array=( ${function_input} );
  fi

  # Performs path check on all deps listed in dep_array
  for dep in ${dep_array[@]}; do
    which ${dep} &>/dev/null \
      || { echo "[FAIL] Dependency (${dep}) missing!" >&2; return 1; };
  done

}


#Yaml Parser
parse_yaml() {
    local yaml_file=$1
    local prefix=$2
    local s
    local w
    local fs

    s='[[:space:]]*'
    w='[a-zA-Z0-9_.-]*'
    fs="$(echo @|tr @ '\034')"

    (
        sed -e '/- [^\“]'"[^\']"'.*: /s|\([ ]*\)- \([[:space:]]*\)|\1-\'$'\n''  \1\2|g' |

        sed -ne '/^--/s|--||g; s|\"|\\\"|g; s/[[:space:]]*$//g;' \
            -e "/#.*[\"\']/!s| #.*||g; /^#/s|#.*||g;" \
            -e "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
            -e "s|^\($s\)\($w\)${s}[:-]$s\(.*\)$s\$|\1$fs\2$fs\3|p" |

        awk -F"$fs" '{
            indent = length($1)/2;
            if (length($2) == 0) { conj[indent]="+";} else {conj[indent]="";}
            vname[indent] = $2;
            for (i in vname) {if (i > indent) {delete vname[i]}}
                if (length($3) > 0) {
                    vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
                    printf("%s%s%s%s=(\"%s\")\n", "'"$prefix"'",vn, $2, conj[indent-1],$3);
                }
            }' |

        sed -e 's/_=/+=/g' |

        awk 'BEGIN {
                FS="=";
                OFS="="
            }
            /(-|\.).*=/ {
                gsub("-|\\.", "_", $1)
            }
            { print }'
    ) < "$yaml_file"
}
